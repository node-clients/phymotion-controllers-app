import { useContext, useEffect, useMemo, useState, useRef } from "react";
import Plot from "react-plotly.js";
import { ClientContext } from "../App";


export const PositionHistory = () => {

    const motor = useContext(ClientContext)
   
    const [positionHistory, setPositionHistory] = useState<any>({ positions : [], timestamp : [] });
    const movementCompleteEvent = useRef<EventSource>(null)

    const fetchPositionHistory = async () => {
        let posHist = positionHistory
        if(motor) {
            posHist = await (await motor.readProperty('position_history')).value()
        }
        setPositionHistory(posHist)
    }

    useEffect(() => {
        fetchPositionHistory()
        if(!motor) return
        motor.observeProperty('is_moving', (eventData : any) => {
            console.log("movement status changed" , eventData)
            fetchPositionHistory()
        }).then((eventSource : EventSource) => {
            console.log("subscribed to movement status change event")
            movementCompleteEvent.current = eventSource
        })
    }, [motor])

    const data = [{
            x: positionHistory.timestamp,
            y: positionHistory.positions,
            mode: "lines+markers",
        },
    ];

    const layout = useMemo(() => {
        return ({ 
            title: "Position History",
            xaxis: {
                title: 'time (HH:MM:SS.fff)',
                tickangle: -45
            },
            yaxis: {
                title: 'position (from motor settings)'
            },
            margin : {
                l: 50,
                r: 15,
                t: 100, 
                b: 75
            },
        })
    }, []);

    const config = useMemo(() => {
        return ({ 
            responsive: true
        })
    }, []);
    
    return (
        <Plot 
            style={{ position: 'absolute', width : "100%", height: "100%"}}
            data={data} 
            layout={layout} 
            config={config} 
        />
        
    )
  
}