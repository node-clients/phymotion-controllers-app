import { useContext, useEffect, useState } from "react";
import { Autocomplete, Box, IconButton, List, ListItem, ListItemText, ListSubheader, Checkbox,
                Modal, Stack, TextField, Typography } from "@mui/material"
import InfoIcon from '@mui/icons-material/Info';
import SendIcon from '@mui/icons-material/Send';
import RefreshIcon from '@mui/icons-material/Refresh';

import { modalStyle, Padding } from "../layout/styles";
import { getCamelCaseName, underscoreNames } from "../interactions/properties";
import { ClientContext, TDContext, SettingsPropertiesContext } from "../App";




const SettingDoc = ({title, doc, open, setOpen} : 
        {title : string, doc : string, open : boolean, setOpen : Function}) => {

    const handleClose = () => {
        setOpen(false);
    };

    return (
        <Modal
            open={open}
            onClose={handleClose}   
        >
            <Box sx={modalStyle} >
                <Typography variant="h6">{title}</Typography>
                <Typography sx={{ paddingTop : 1 }}>{doc ? doc : "No doc or description provided"}</Typography>
            </Box>
        </Modal>

    )
} 

const StringSetting = (
    { prop, requestedValue, setTypeCastedValue } : 
    { prop : any , requestedValue : any, setTypeCastedValue : Function}
) => {
    return (
        <TextField 
            sx={{ minWidth: 150 , maxWidth: 150 }} 
            value={requestedValue} 
            onChange={(event) => setTypeCastedValue(event.target.value)}
        />        
    )
}


const NumberSetting = (
    { prop, requestedValue, setTypeCastedValue } : 
    { prop : any , requestedValue : any, setTypeCastedValue : Function}
) => {

    return (
        <TextField 
            type="number" 
            sx={{ minWidth: 150 , maxWidth: 150 }}
            value={requestedValue} 
            onChange={(event) => setTypeCastedValue(event.target.value)}
        />
    )
}

const EnumSetting = (
    { prop, requestedValue, setTypeCastedValue } : 
    { prop : any , requestedValue : any, setTypeCastedValue : Function}
) => {

    return (
        <Autocomplete 
            disablePortal
            options={prop.enum.map((option: any) => String(option))}
            value={String(requestedValue)}
            sx={{ minWidth: 150 , maxWidth: 150 }}
            onChange={(event, value, reason) => setTypeCastedValue(value)}
            renderInput={(params) => <TextField {...params} /> }
        />
    )
}

const BooleanSetting = (
    { prop, requestedValue, setTypeCastedValue } : 
    { prop : any , requestedValue : any, setTypeCastedValue : Function}
) => {

    return (
        <Checkbox checked={requestedValue} onChange={(event) => setTypeCastedValue(event.target.checked)} />
    )
}


const UnimplementedSetting = (
    { prop, requestedValue, setTypeCastedValue } : 
    { prop : any , requestedValue : any, setTypeCastedValue : Function}
) => {

    return (
        <div>Unimplemented Setting</div>
    )
}


const getElement = (dataSchema : any) : any => {

    let typeCast, element

    if(dataSchema.oneOf) {
        for(let type of dataSchema.oneOf) {
            if(type.type !== 'null') {
                [element, typeCast] = getElement(type)
                break
            }
        }
    } else {
        switch(dataSchema.type) {
            case "string" : element = StringSetting, typeCast = String; break;
            case "integer": 
            case "number" : element =  dataSchema.enum? EnumSetting : NumberSetting;
                            // all enumerations are numbers, check TD
                            typeCast = Number; break;
            case "boolean" : element = BooleanSetting; typeCast = Boolean; break;
            default : element = UnimplementedSetting; typeCast = (value : any) => value;
            }
    }
    
    return [element, typeCast]
}

const Setting = ({ name, prop } : { name : string, prop : any }) => {

    const [element, typeCast] = getElement(prop)
    const motor = useContext(ClientContext)
    const totalReadValue = useContext(SettingsPropertiesContext)
    const [readValue, setReadValue] = useState<any>(totalReadValue[getCamelCaseName(name)])
    const [requestedValue, setRequestedValue] = useState<any>(typeCast(readValue))
    const [openDoc, setOpenDoc] = useState<boolean>(false)

    useEffect(() => {
        let value = typeCast(totalReadValue[getCamelCaseName(name)])
        setReadValue(value)
        setRequestedValue(value)
    }, [totalReadValue[getCamelCaseName(name)]])

    const sendProperty = async() => {
        await motor.writeProperty(name, typeCast(requestedValue))
        let newReadValue = await (await motor.readProperty(name)).value()
        totalReadValue[name] = newReadValue
        setReadValue(newReadValue)
        setRequestedValue(newReadValue)
    }

    const resetProperty = () => {
        setRequestedValue(typeCast(readValue))
    }

    const setTypeCastedValue = (value : any) => {
        setRequestedValue(typeCast(value))
    }

    return (
        <Stack direction="row" sx={{ alignItems: 'center' }}>
            <Typography sx={{ minWidth: 100, maxWidth: 100, paddingRight: 0.5}}>{prop.title.replace(/_/g, ' ')}</Typography>
            {element({prop, requestedValue, setTypeCastedValue})}            
            <IconButton size="small" onClick={() => setOpenDoc(true)}>
                <InfoIcon fontSize="small" />
            </IconButton>
            <SettingDoc title={prop.title.replace(/_/g, ' ')} open={openDoc} setOpen={setOpenDoc} doc={prop.description} />
            {readValue !== requestedValue ? 
                <Stack direction="row">
                    <IconButton sx={{ backgroundColor: '#F2F2F2' }} onClick={sendProperty}>
                        <SendIcon />
                    </IconButton>
                    <Padding value={0.25} />
                    <IconButton sx={{ backgroundColor: '#F2F2F2' }} onClick={resetProperty}>
                        <RefreshIcon />
                    </IconButton>
                </Stack>
                :
                null
            }
        </Stack>
    )

}



export const MotorSettings =  () => {
    
    const [TD, _] = useContext(TDContext) 

    return (
        <List
            sx={{
                maxWidth: '97%',
                padding : 1,
                bgcolor: 'background.paper',
                position: 'relative',
                overflowY: 'auto',
                overflowX : 'none',
                '& ul': { padding: 0 },
            }}
            subheader={<li />}
        >
            {Object.keys(TD.properties).map((propname : any) => {
                let prop = TD.properties[propname as keyof typeof TD]
                if(!(underscoreNames.includes(propname))) {
                    // console.debug("skipping", propname) // this component is re-rending, try to optimize it
                    return (<div key={prop.title}></div>)
                }
                return (
                    <ListItem key={prop.title}>
                        <Setting name={propname} prop={prop} />
                    </ListItem>
                )
            })} 
        </List>
    )
}

