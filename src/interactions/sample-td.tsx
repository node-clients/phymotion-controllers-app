export const sampleTD = {
    "@context": "https://www.w3.org/2022/wot/td/v1.1",
    "id": "http://localhost:8080/linear-stage",
    "title": "Axis",
    "description": "no class doc provided",
    "properties": {
      "state": {
        "title": "state",
        "description": "current state machine's state if state machine present, None indicates absence of state machine.",
        "readOnly": true,
        "type": "string",
        "forms": [
          {
            "href": "http://localhost:8080/linear-stage/state",
            "op": "readproperty",
            "htv:methodName": "GET",
            "contentType": "application/json"
          }
        ]
      },
      "drive_ID": {
        "title": "drive_ID",
        "description": "module number within the controller stack (1-16)",
        "default": 1,
        "type": "integer",
        "forms": [
          {
            "href": "http://localhost:8080/linear-stage/module/id",
            "op": "readproperty",
            "htv:methodName": "GET",
            "contentType": "application/json"
          },
          {
            "href": "http://localhost:8080/linear-stage/module/id",
            "op": "writeproperty",
            "htv:methodName": "PUT",
            "contentType": "application/json"
          }
        ],
        "minimum": 1,
        "maximum": 16
      },
      "axis": {
        "title": "axis",
        "description": "Axis on module (1-4), always 1 for single axis I1AM01 modules",
        "default": 1,
        "type": "integer",
        "forms": [
          {
            "href": "http://localhost:8080/linear-stage/channel",
            "op": "readproperty",
            "htv:methodName": "GET",
            "contentType": "application/json"
          },
          {
            "href": "http://localhost:8080/linear-stage/channel",
            "op": "writeproperty",
            "htv:methodName": "PUT",
            "contentType": "application/json"
          }
        ],
        "minimum": 1,
        "maximum": 4
      },
      "movement_type": {
        "title": "movement_type",
        "description": "Type of movement (free run, relative / absolute, reference run)0 = ignore limit switches, for example with rotational movement,1 = use hardware limit switches, for example, with XY tables or otherlinear systems, 2 limit switches: limit direction minus, Limit direction plus,2 = software limit switches are monitored,3 = both hardware and software limit switches are monitored,",
        "type": "integer",
        "enum": [
          0,
          1,
          2,
          3
        ],
        "forms": [
          {
            "href": "http://localhost:8080/linear-stage/movement-type",
            "op": "readproperty",
            "htv:methodName": "GET",
            "contentType": "application/json"
          },
          {
            "href": "http://localhost:8080/linear-stage/movement-type",
            "op": "writeproperty",
            "htv:methodName": "PUT",
            "contentType": "application/json"
          }
        ]
      },
      "position_units": {
        "title": "position_units",
        "description": "Measuring units of movement: used by hardware but only for displaying1 = step, 2 = mm, 3 = inch, 4 = degree",
        "type": "integer",
        "enum": [
          1,
          2,
          3,
          4
        ],
        "forms": [
          {
            "href": "http://localhost:8080/linear-stage/position-units",
            "op": "readproperty",
            "htv:methodName": "GET",
            "contentType": "application/json"
          },
          {
            "href": "http://localhost:8080/linear-stage/position-units",
            "op": "writeproperty",
            "htv:methodName": "PUT",
            "contentType": "application/json"
          }
        ]
      },
      "conversion_factor": {
        "title": "conversion_factor",
        "description": "The conversion factor, distance=conversion*steps, steps=distance/conversion.Example: 1mm travel per 400steps: conversion=1/400=0.0025, Caution: When changingthe step resolution e.g. from 1/4 to 1/2, this factor also has to change.",
        "type": "number",
        "forms": [
          {
            "href": "http://localhost:8080/linear-stage/conversion-factor",
            "op": "readproperty",
            "htv:methodName": "GET",
            "contentType": "application/json"
          },
          {
            "href": "http://localhost:8080/linear-stage/conversion-factor",
            "op": "writeproperty",
            "htv:methodName": "PUT",
            "contentType": "application/json"
          }
        ],
        "exclusiveMinimum": 0
      },
      "start_stop_frequency": {
        "title": "start_stop_frequency",
        "description": "Start and Stop frequency in Hz with which to start the movement. Default = 400",
        "type": "integer",
        "forms": [
          {
            "href": "http://localhost:8080/linear-stage/frequencies/start-stop",
            "op": "readproperty",
            "htv:methodName": "GET",
            "contentType": "application/json"
          },
          {
            "href": "http://localhost:8080/linear-stage/frequencies/start-stop",
            "op": "writeproperty",
            "htv:methodName": "PUT",
            "contentType": "application/json"
          }
        ],
        "exclusiveMinimum": 0
      },
      "emergency_stop_ramp": {
        "title": "emergency_stop_ramp",
        "description": "Emergency stop ramp, Input for I1AM0x: in 4000 Hz/s steps",
        "type": "number",
        "forms": [
          {
            "href": "http://localhost:8080/linear-stage/frequencies/emergency-stop-ramp",
            "op": "readproperty",
            "htv:methodName": "GET",
            "contentType": "application/json"
          },
          {
            "href": "http://localhost:8080/linear-stage/frequencies/emergency-stop-ramp",
            "op": "writeproperty",
            "htv:methodName": "PUT",
            "contentType": "application/json"
          }
        ],
        "exclusiveMinimum": 4000
      },
      "referencing_run_frequency": {
        "title": "referencing_run_frequency",
        "description": "Run frequency during initializing (referencing), in Hz (integer value).I1AM0x: 40 000 maximum, I4XM01: 4 000 000 maximum",
        "type": "number",
        "forms": [
          {
            "href": "http://localhost:8080/linear-stage/frequencies/referencing-run",
            "op": "readproperty",
            "htv:methodName": "GET",
            "contentType": "application/json"
          },
          {
            "href": "http://localhost:8080/linear-stage/frequencies/referencing-run",
            "op": "writeproperty",
            "htv:methodName": "PUT",
            "contentType": "application/json"
          }
        ],
        "maximum": 40000,
        "exclusiveMinimum": 0
      },
      "referencing_acceleration_frequency": {
        "title": "referencing_acceleration_frequency",
        "description": "Ramp during initializing, associated to parameter P08 (ReferencingRunFrequency).Input for I1AM0x: in 4000 Hz/s steps, I4XM01: in 1 Hz/s steps, Default = 4000",
        "type": "number",
        "forms": [
          {
            "href": "http://localhost:8080/linear-stage/frequencies/referencing-acceleration",
            "op": "readproperty",
            "htv:methodName": "GET",
            "contentType": "application/json"
          },
          {
            "href": "http://localhost:8080/linear-stage/frequencies/referencing-acceleration",
            "op": "writeproperty",
            "htv:methodName": "PUT",
            "contentType": "application/json"
          }
        ],
        "maximum": 40000,
        "exclusiveMinimum": 0
      },
      "referencing_leave_frequency": {
        "title": "referencing_leave_frequency",
        "description": "Run frequency for leaving the limit switchin Hz (integer value)Default = 400",
        "type": "number",
        "forms": [
          {
            "href": "http://localhost:8080/linear-stage/frequencies/referencing-leave",
            "op": "readproperty",
            "htv:methodName": "GET",
            "contentType": "application/json"
          },
          {
            "href": "http://localhost:8080/linear-stage/frequencies/referencing-leave",
            "op": "writeproperty",
            "htv:methodName": "PUT",
            "contentType": "application/json"
          }
        ],
        "maximum": 40000,
        "exclusiveMinimum": 0
      },
      "referencing_limit_plus_offset": {
        "title": "referencing_limit_plus_offset",
        "description": "offset for limit switch direction + (away from 'LIMIT+' switch,towards 'LIMIT–' switch)Distance between reference point M0P and limit switch activationUnit: as defined in parameter P02 (PositionUnits)Value >= 0Default = 0",
        "type": "number",
        "forms": [
          {
            "href": "http://localhost:8080/linear-stage/referencing-limits/plus-offset",
            "op": "readproperty",
            "htv:methodName": "GET",
            "contentType": "application/json"
          },
          {
            "href": "http://localhost:8080/linear-stage/referencing-limits/plus-offset",
            "op": "writeproperty",
            "htv:methodName": "PUT",
            "contentType": "application/json"
          }
        ],
        "minimum": 0
      },
      "referencing_limit_minus_offset": {
        "title": "referencing_limit_minus_offset",
        "description": "offset for limit switch direction + (away from 'LIMIT+' switch,towards 'LIMIT–' switch)Distance between reference point M0P and limit switch activationUnit: as defined in parameter P02 (PositionUnits)Value >= 0Default = 0",
        "type": "number",
        "forms": [
          {
            "href": "http://localhost:8080/linear-stage/referencing-limits/minus-offset",
            "op": "readproperty",
            "htv:methodName": "GET",
            "contentType": "application/json"
          },
          {
            "href": "http://localhost:8080/linear-stage/referencing-limits/minus-offset",
            "op": "writeproperty",
            "htv:methodName": "PUT",
            "contentType": "application/json"
          }
        ],
        "minimum": 0
      },
      "referencing_recovery_time": {
        "title": "referencing_recovery_time",
        "description": "Time lapse during initialization in msec, default 20msec",
        "type": "integer",
        "forms": [
          {
            "href": "http://localhost:8080/linear-stage/referencing-recovery-time",
            "op": "readproperty",
            "htv:methodName": "GET",
            "contentType": "application/json"
          },
          {
            "href": "http://localhost:8080/linear-stage/referencing-recovery-time",
            "op": "writeproperty",
            "htv:methodName": "PUT",
            "contentType": "application/json"
          }
        ],
        "minimum": 0
      },
      "run_frequency": {
        "title": "run_frequency",
        "description": "Run frequency during program operation. Enter in Hz (integer value)I1AM0x: 40 000 maximum, I4XM01: 4 000 000 maximum, Default = 4000",
        "type": "integer",
        "forms": [
          {
            "href": "http://localhost:8080/linear-stage/run_frequency",
            "op": "readproperty",
            "htv:methodName": "GET",
            "contentType": "application/json"
          },
          {
            "href": "http://localhost:8080/linear-stage/run_frequency",
            "op": "writeproperty",
            "htv:methodName": "PUT",
            "contentType": "application/json"
          }
        ],
        "exclusiveMinimum": 0
      },
      "acceleration": {
        "title": "acceleration",
        "description": "Ramp for run frequency in Hz/(sec steps)I1AM0x: in 4000 Hz/s stepsI4XM01: in 1 Hz/s stepsDefault = 4000",
        "type": "integer",
        "forms": [
          {
            "href": "http://localhost:8080/linear-stage/acceleration",
            "op": "readproperty",
            "htv:methodName": "GET",
            "contentType": "application/json"
          },
          {
            "href": "http://localhost:8080/linear-stage/acceleration",
            "op": "writeproperty",
            "htv:methodName": "PUT",
            "contentType": "application/json"
          }
        ],
        "exclusiveMinimum": 0
      },
      "position_recovery_time": {
        "title": "position_recovery_time",
        "description": "Time lapse during after positioning in msec.",
        "type": "integer",
        "forms": [
          {
            "href": "http://localhost:8080/linear-stage/position/recovery_time",
            "op": "readproperty",
            "htv:methodName": "GET",
            "contentType": "application/json"
          },
          {
            "href": "http://localhost:8080/linear-stage/position/recovery_time",
            "op": "writeproperty",
            "htv:methodName": "PUT",
            "contentType": "application/json"
          }
        ],
        "minimum": 1
      },
      "boost_enable": {
        "title": "boost_enable",
        "description": "Boost (current is defined in P42, BoostCurrent)0 = off1 = on during motor run2 = on during acceleration and deceleration ramp",
        "type": "boolean",
        "forms": [
          {
            "href": "http://localhost:8080/linear-stage/boost-enable",
            "op": "readproperty",
            "htv:methodName": "GET",
            "contentType": "application/json"
          },
          {
            "href": "http://localhost:8080/linear-stage/boost-enable",
            "op": "writeproperty",
            "htv:methodName": "PUT",
            "contentType": "application/json"
          }
        ]
      },
      "encoder_deviation_counter": {
        "title": "encoder_deviation_counter",
        "type": "number",
        "forms": [
          {
            "href": "http://localhost:8080/linear-stage/encoder/deviation-counter",
            "op": "readproperty",
            "htv:methodName": "GET",
            "contentType": "application/json"
          },
          {
            "href": "http://localhost:8080/linear-stage/encoder/deviation-counter",
            "op": "writeproperty",
            "htv:methodName": "PUT",
            "contentType": "application/json"
          }
        ]
      },
      "position": {
        "title": "position",
        "description": "current position, write operation does not cause movement but overwrite register value.",
        "type": "number",
        "forms": [
          {
            "href": "http://localhost:8080/linear-stage/position",
            "op": "readproperty",
            "htv:methodName": "GET",
            "contentType": "application/json"
          },
          {
            "href": "http://localhost:8080/linear-stage/position",
            "op": "writeproperty",
            "htv:methodName": "PUT",
            "contentType": "application/json"
          }
        ]
      },
      "absolute_counter": {
        "title": "absolute_counter",
        "description": "Encoder, multi turn and also for single turn. The value of P22 is extended to P21 by software.The encoder counters have a fixed resolution, e.g. 10 bit (for single-turn encoders: the resolution isbits per turn), then the read value repeats. A saw tooth profile of the the numerical values isproduced during a continuous motor running. This course is \"straightened\" by software. P20 and P21 will bescaled to the same value per revolution by P3 and P39 and are therefore directly comparable, see P36.",
        "type": "number",
        "forms": [
          {
            "href": "http://localhost:8080/linear-stage/absolute-counter",
            "op": "readproperty",
            "htv:methodName": "GET",
            "contentType": "application/json"
          },
          {
            "href": "http://localhost:8080/linear-stage/absolute-counter",
            "op": "writeproperty",
            "htv:methodName": "PUT",
            "contentType": "application/json"
          }
        ]
      },
      "encoder_counter": {
        "title": "encoder_counter",
        "description": "Indicates the true absolute encoder position. Is only set for A/B encoders to zero (after reset),the absolute encoder remains the value.",
        "default": 0,
        "type": "integer",
        "forms": [
          {
            "href": "http://localhost:8080/linear-stage/encoder_counter",
            "op": "readproperty",
            "htv:methodName": "GET",
            "contentType": "application/json"
          },
          {
            "href": "http://localhost:8080/linear-stage/encoder_counter",
            "op": "writeproperty",
            "htv:methodName": "PUT",
            "contentType": "application/json"
          }
        ]
      },
      "soft_limit_max": {
        "title": "soft_limit_max",
        "description": "Software limit maximum, 0=no limitation",
        "type": "number",
        "forms": [
          {
            "href": "http://localhost:8080/linear-stage/software-limit/max",
            "op": "readproperty",
            "htv:methodName": "GET",
            "contentType": "application/json"
          },
          {
            "href": "http://localhost:8080/linear-stage/software-limit/max",
            "op": "writeproperty",
            "htv:methodName": "PUT",
            "contentType": "application/json"
          }
        ]
      },
      "soft_limit_min": {
        "title": "soft_limit_min",
        "description": "Software limit minimum, 0=no limitation",
        "type": "number",
        "forms": [
          {
            "href": "http://localhost:8080/linear-stage/software-limits/min",
            "op": "readproperty",
            "htv:methodName": "GET",
            "contentType": "application/json"
          },
          {
            "href": "http://localhost:8080/linear-stage/software-limits/min",
            "op": "writeproperty",
            "htv:methodName": "PUT",
            "contentType": "application/json"
          }
        ]
      },
      "play_compensation": {
        "title": "play_compensation",
        "description": "Indicates the distance, the target position in the selected directionis passed over and afterwards is started in reverse direction. 0 = compensation off.",
        "type": "number",
        "forms": [
          {
            "href": "http://localhost:8080/linear-stage/play-compensation",
            "op": "readproperty",
            "htv:methodName": "GET",
            "contentType": "application/json"
          },
          {
            "href": "http://localhost:8080/linear-stage/play-compensation",
            "op": "writeproperty",
            "htv:methodName": "PUT",
            "contentType": "application/json"
          }
        ],
        "minimum": 0
      },
      "encoder_data_rate": {
        "title": "encoder_data_rate",
        "description": "The data transfer rate is set by P26 (ONLY for SSIencoder), by which the encoder is read. The transferrate is dependent on the length of the cable by whichthe encoder is connected to the device. The shorter thecable, the encoder can more quickly be read.Data transfer rate 1 to 10 (= 100 to 1000 kHz)1 = 100 kHz2 = 200 kHz3 = 300 kHz4 = 400 kHz5 = 500 kHz6 = 600 kHz7 = 700 kHz8 = 800 kHz9 = 900 kHz10 = 1000 kHzDefault = 1 = 100kHz",
        "type": "integer",
        "forms": [
          {
            "href": "http://localhost:8080/linear-stage/encoder/data-rate",
            "op": "readproperty",
            "htv:methodName": "GET",
            "contentType": "application/json"
          },
          {
            "href": "http://localhost:8080/linear-stage/encoder/data-rate",
            "op": "writeproperty",
            "htv:methodName": "PUT",
            "contentType": "application/json"
          }
        ],
        "minimum": 0,
        "maximum": 10
      },
      "limit_switch_type": {
        "title": "limit_switch_type",
        "description": "Limit switch type, NCC: normally closed contact,NOC: normally open contactLIM–  Center/Ref  LIM+0   NCC     NCC     NCC1   NCC     NCC     NOC2   NOC     NCC     NCC3   NOC     NCC     NOC4   NCC     NOC     NCC5   NCC     NOC     NOC6   NOC     NOC     NCC7   NOC     NOC     NOC",
        "type": "integer",
        "forms": [
          {
            "href": "http://localhost:8080/linear-stage/limit-switch/type",
            "op": "readproperty",
            "htv:methodName": "GET",
            "contentType": "application/json"
          },
          {
            "href": "http://localhost:8080/linear-stage/limit-switch/type",
            "op": "writeproperty",
            "htv:methodName": "PUT",
            "contentType": "application/json"
          }
        ],
        "minimum": 0,
        "maximum": 7
      },
      "enabled": {
        "title": "enabled",
        "description": "enable or disable axis for moving",
        "type": "boolean",
        "forms": [
          {
            "href": "http://localhost:8080/linear-stage/enabled",
            "op": "readproperty",
            "htv:methodName": "GET",
            "contentType": "application/json"
          },
          {
            "href": "http://localhost:8080/linear-stage/enabled",
            "op": "writeproperty",
            "htv:methodName": "PUT",
            "contentType": "application/json"
          }
        ]
      },
      "enabled_at_boot": {
        "title": "enabled_at_boot",
        "description": "0 = Power stage is deactivated after power on, 1 = Power stage is activated after power on,Default = On for directly connected modules, off for modules on separate powersupply.",
        "type": "boolean",
        "forms": [
          {
            "href": "http://localhost:8080/linear-stage/enabled-at-boot",
            "op": "readproperty",
            "htv:methodName": "GET",
            "contentType": "application/json"
          },
          {
            "href": "http://localhost:8080/linear-stage/enabled-at-boot",
            "op": "writeproperty",
            "htv:methodName": "PUT",
            "contentType": "application/json"
          }
        ]
      },
      "frequency_band": {
        "title": "frequency_band",
        "description": "For I4XM01 only! Frequency band setting, 0 = manual, 1 = automatic,Default = 1. Remark: It is recommended to work with the automatic setting mode.For each run frequency (P14) and ramp (P15) the controller automatically selects suitable settings.",
        "type": "integer",
        "enum": [
          0,
          1
        ],
        "forms": [
          {
            "href": "http://localhost:8080/linear-stage/frequency-band",
            "op": "readproperty",
            "htv:methodName": "GET",
            "contentType": "application/json"
          },
          {
            "href": "http://localhost:8080/linear-stage/frequency-band",
            "op": "writeproperty",
            "htv:methodName": "PUT",
            "contentType": "application/json"
          }
        ]
      },
      "control_predivider": {
        "title": "control_predivider",
        "description": "For I4XM01 only! Frequency and ramp predivider (only if P30 = 0, manual)Values = 0-9, default = 3, This parameter changes the predivider which suppliesthe hardware (frequency generated) with a clock of 20MHz derived.",
        "type": "integer",
        "forms": [
          {
            "href": "http://localhost:8080/linear-stage/control-predivider",
            "op": "readproperty",
            "htv:methodName": "GET",
            "contentType": "application/json"
          },
          {
            "href": "http://localhost:8080/linear-stage/control-predivider",
            "op": "writeproperty",
            "htv:methodName": "PUT",
            "contentType": "application/json"
          }
        ]
      },
      "acceleration_ramp_shape": {
        "title": "acceleration_ramp_shape",
        "type": "integer",
        "forms": [
          {
            "href": "http://localhost:8080/linear-stage/acceleration_ramp_shape",
            "op": "readproperty",
            "htv:methodName": "GET",
            "contentType": "application/json"
          },
          {
            "href": "http://localhost:8080/linear-stage/acceleration_ramp_shape",
            "op": "writeproperty",
            "htv:methodName": "PUT",
            "contentType": "application/json"
          }
        ]
      },
      "acceleration_ramp_arc": {
        "title": "acceleration_ramp_arc",
        "description": "Arc value setting for s-shape ramp (higher is steeper)Values: OMC: 1 to 8191, TMC: 1 to 32767. Default = 1",
        "default": 1,
        "type": "integer",
        "forms": [
          {
            "href": "http://localhost:8080/linear-stage/acceleration-ramp-arc",
            "op": "readproperty",
            "htv:methodName": "GET",
            "contentType": "application/json"
          },
          {
            "href": "http://localhost:8080/linear-stage/acceleration-ramp-arc",
            "op": "writeproperty",
            "htv:methodName": "PUT",
            "contentType": "application/json"
          }
        ],
        "minimum": 1,
        "maximum": 32767
      },
      "encoder_type": {
        "title": "encoder_type",
        "description": "Encoder type, 0 = no encoder, 1 = incremental 5.0 V, 2 = incremental 5.5 V,3 = serial interface SSI binary Code 5.0 V, 4 = serial interface SSI binary Code 5.5 V,5 = serial interface SSI Gray Code 5.0 V, 6 = serial interface SSI Gray Code 5.5 V,7 = EnDat 5 V, 8 = EnDat 5.5 V, 9 = resolver, 10 = LVDT 4-wire, 11 = LVDT 5/6-wire,12 = BiSS 5,0 V, 13 = BiSS 24,0 V, Default = 0",
        "type": "integer",
        "forms": [
          {
            "href": "http://localhost:8080/linear-stage/encoder/bounds",
            "op": "readproperty",
            "htv:methodName": "GET",
            "contentType": "application/json"
          },
          {
            "href": "http://localhost:8080/linear-stage/encoder/bounds",
            "op": "writeproperty",
            "htv:methodName": "PUT",
            "contentType": "application/json"
          }
        ],
        "minimum": 0,
        "maximum": 13
      },
      "encoder_resolution": {
        "title": "encoder_resolution",
        "description": "Encoder resolution for SSI and EnDat encoder.",
        "type": "integer",
        "forms": [
          {
            "href": "http://localhost:8080/linear-stage/encoder/resolution",
            "op": "readproperty",
            "htv:methodName": "GET",
            "contentType": "application/json"
          },
          {
            "href": "http://localhost:8080/linear-stage/encoder/resolution",
            "op": "writeproperty",
            "htv:methodName": "PUT",
            "contentType": "application/json"
          }
        ],
        "minimum": 1,
        "maximum": 24
      },
      "encoder_function": {
        "title": "encoder_function",
        "description": "This parameter specifies the use of P21 as a pure counter or whether its valueis continuously compared with the value of the P20 counter, if the counter valuesvary too much, the motion is aborted with an error message. 0 = counter,1 = counter+SFI.",
        "type": "integer",
        "forms": [
          {
            "href": "http://localhost:8080/linear-stage/encoder/usage",
            "op": "readproperty",
            "htv:methodName": "GET",
            "contentType": "application/json"
          },
          {
            "href": "http://localhost:8080/linear-stage/encoder/usage",
            "op": "writeproperty",
            "htv:methodName": "PUT",
            "contentType": "application/json"
          }
        ],
        "minimum": 0,
        "maximum": 1
      },
      "encoder_SFI_tolerance": {
        "title": "encoder_SFI_tolerance",
        "description": "Encoder tolerance for SFI, Enter tolerance value for SFI evaluation.Check documentation, Default = 0",
        "type": "number",
        "forms": [
          {
            "href": "http://localhost:8080/linear-stage/encoder/sfi-tolerance",
            "op": "readproperty",
            "htv:methodName": "GET",
            "contentType": "application/json"
          },
          {
            "href": "http://localhost:8080/linear-stage/encoder/sfi-tolerance",
            "op": "writeproperty",
            "htv:methodName": "PUT",
            "contentType": "application/json"
          }
        ],
        "minimum": 0
      },
      "encoder_preferential_direction": {
        "title": "encoder_preferential_direction",
        "description": "Encoder preferential direction of rotation, 0 = + (positive), 1 = – (negative),Default = 0",
        "type": "integer",
        "forms": [
          {
            "href": "http://localhost:8080/linear-stage/encoder/preferential-direction",
            "op": "readproperty",
            "htv:methodName": "GET",
            "contentType": "application/json"
          },
          {
            "href": "http://localhost:8080/linear-stage/encoder/preferential-direction",
            "op": "writeproperty",
            "htv:methodName": "PUT",
            "contentType": "application/json"
          }
        ],
        "minimum": 0,
        "maximum": 1
      },
      "encoder_conversion": {
        "title": "encoder_conversion",
        "description": "Encoder conversion factor, 1 increment corresponds to ...units, Conversion = Thread/Enc-Steps-per-revDefault = 1",
        "type": "number",
        "forms": [
          {
            "href": "http://localhost:8080/linear-stage/encoder/conversion",
            "op": "readproperty",
            "htv:methodName": "GET",
            "contentType": "application/json"
          },
          {
            "href": "http://localhost:8080/linear-stage/encoder/conversion",
            "op": "writeproperty",
            "htv:methodName": "PUT",
            "contentType": "application/json"
          }
        ],
        "exclusiveMinimum": 0
      },
      "stop_current": {
        "title": "stop_current",
        "description": "Stop current in [A]",
        "type": "number",
        "forms": [
          {
            "href": "http://localhost:8080/linear-stage/stop-current",
            "op": "readproperty",
            "htv:methodName": "GET",
            "contentType": "application/json"
          },
          {
            "href": "http://localhost:8080/linear-stage/stop-current",
            "op": "writeproperty",
            "htv:methodName": "PUT",
            "contentType": "application/json"
          }
        ],
        "minimum": 0,
        "maximum": 2.5
      },
      "run_current": {
        "title": "run_current",
        "description": "Run current in [A]",
        "type": "number",
        "forms": [
          {
            "href": "http://localhost:8080/linear-stage/run-current",
            "op": "readproperty",
            "htv:methodName": "GET",
            "contentType": "application/json"
          },
          {
            "href": "http://localhost:8080/linear-stage/run-current",
            "op": "writeproperty",
            "htv:methodName": "PUT",
            "contentType": "application/json"
          }
        ],
        "minimum": 0,
        "maximum": 2.5
      },
      "boost_current": {
        "title": "boost_current",
        "description": "Boost current in [A]",
        "type": "number",
        "forms": [
          {
            "href": "http://localhost:8080/linear-stage/boost-current",
            "op": "readproperty",
            "htv:methodName": "GET",
            "contentType": "application/json"
          },
          {
            "href": "http://localhost:8080/linear-stage/boost-current",
            "op": "writeproperty",
            "htv:methodName": "PUT",
            "contentType": "application/json"
          }
        ],
        "minimum": 0,
        "maximum": 2.5
      },
      "runcurrent_hold_time": {
        "title": "runcurrent_hold_time",
        "description": "Current hold time in msec after positioning.Caution! Labeled differently in manuals, sometimes\"Stoppstromüberhöhungszeit\" (stop current boost time) and sometimes\"Laufstromüberhöhungszeit\" (run current boost time).After finishing positioning, hold at \"RunCurrent\" for X msec beforeswitching to \"StopCurrent\". Default = 20.",
        "type": "integer",
        "forms": [
          {
            "href": "http://localhost:8080/linear-stage/run-current/hold-time",
            "op": "readproperty",
            "htv:methodName": "GET",
            "contentType": "application/json"
          },
          {
            "href": "http://localhost:8080/linear-stage/run-current/hold-time",
            "op": "writeproperty",
            "htv:methodName": "PUT",
            "contentType": "application/json"
          }
        ],
        "exclusiveMinimum": 0
      },
      "control_pulse_origin": {
        "title": "control_pulse_origin",
        "description": "For I4XM01 only, Origin of the control pulses of the axis,0: 1:1 (Input = Output), 1 : from X, 2 : from Y, 3 : from Z, 4 : from U,5 : from external",
        "type": "integer",
        "enum": [
          0,
          1,
          2,
          3,
          4,
          5
        ],
        "forms": [
          {
            "href": "http://localhost:8080/linear-stage/control-pulse/origin",
            "op": "readproperty",
            "htv:methodName": "GET",
            "contentType": "application/json"
          },
          {
            "href": "http://localhost:8080/linear-stage/control-pulse/origin",
            "op": "writeproperty",
            "htv:methodName": "PUT",
            "contentType": "application/json"
          }
        ]
      },
      "step_resolution": {
        "title": "step_resolution",
        "description": "Microstepping step resolution. 0 = 1/1 step,1 = 1/2 step, 2 = 1/2.5 step, 3 = 1/4 step, 4 = 1/5 step, 7 = 1/16 step, 5 = 1/8 step,8 = 1/20 step, 9 = 1/32 step, 6 = 1/10 step, 10 = 1/64 step, 11 = 1/128 step, 12 = 1/256 step,13 = 1/512 step (e.g. APS01) Important: for I1AM: step resolution from 1/1 to 1/128 step,this setting only applies to the INTERNAL power stages or power stages, which are connected via a bus.",
        "type": "integer",
        "enum": [
          0,
          1,
          2,
          3,
          4,
          5,
          6,
          7,
          8,
          9,
          10,
          11,
          12,
          13
        ],
        "forms": [
          {
            "href": "http://localhost:8080/linear-stage/step-resolution",
            "op": "readproperty",
            "htv:methodName": "GET",
            "contentType": "application/json"
          },
          {
            "href": "http://localhost:8080/linear-stage/step-resolution",
            "op": "writeproperty",
            "htv:methodName": "PUT",
            "contentType": "application/json"
          }
        ]
      },
      "power_stage_temperature": {
        "title": "power_stage_temperature",
        "description": "Power stage temperature (not motor temperature!) in degrees celcius.",
        "readOnly": true,
        "type": "number",
        "forms": [
          {
            "href": "http://localhost:8080/linear-stage/power-stage-temperature",
            "op": "readproperty",
            "htv:methodName": "GET",
            "contentType": "application/json"
          }
        ]
      },
      "control_divider": {
        "title": "control_divider",
        "description": "only for I4XM01! Divider for Control pulses, Controlpulses_Output = 1/(n+1) * Controlpulses_Input0 : 1/(0+1)=1, 1: 1/(1+1)= 1/2, 2: 1/(2+1) =1/3, 3: 1/(3+1)=1/4, 4: 1/(4+1)=1/5, 5: 1/(5+1)=1/6Default = 0",
        "type": "integer",
        "enum": [
          0,
          1,
          2,
          3,
          4,
          5
        ],
        "forms": [
          {
            "href": "http://localhost:8080/linear-stage/control-pulse/divider",
            "op": "readproperty",
            "htv:methodName": "GET",
            "contentType": "application/json"
          },
          {
            "href": "http://localhost:8080/linear-stage/control-pulse/divider",
            "op": "writeproperty",
            "htv:methodName": "PUT",
            "contentType": "application/json"
          }
        ]
      },
      "pulse_width": {
        "title": "pulse_width",
        "description": "only for I4XM01! Pulse width: (n+1)*100 ns, n: 0...255, e.g. n=19: (19+1)*100 ns=2000 ns= 2µs,-> F max =1/(2*2 µs)=250 kHz",
        "type": "integer",
        "forms": [
          {
            "href": "http://localhost:8080/linear-stage/control-pulse/width",
            "op": "readproperty",
            "htv:methodName": "GET",
            "contentType": "application/json"
          },
          {
            "href": "http://localhost:8080/linear-stage/control-pulse/width",
            "op": "writeproperty",
            "htv:methodName": "PUT",
            "contentType": "application/json"
          }
        ],
        "minimum": 0,
        "maximum": 255
      },
      "power_stage_monitoring": {
        "title": "power_stage_monitoring",
        "description": "Power stage monitoring, 0 = off, 1 = on (default)",
        "type": "boolean",
        "forms": [
          {
            "href": "http://localhost:8080/linear-stage/power-stage-monitoring",
            "op": "readproperty",
            "htv:methodName": "GET",
            "contentType": "application/json"
          },
          {
            "href": "http://localhost:8080/linear-stage/power-stage-monitoring",
            "op": "writeproperty",
            "htv:methodName": "PUT",
            "contentType": "application/json"
          }
        ]
      },
      "motor_warning_temperature": {
        "title": "motor_warning_temperature",
        "description": "Temperature (in degree Celcius) above which a warning occurs.If the motor warmed up to a defined temperature value,a warning occurs. We recommend to operate the motoronly when it is cooled again.",
        "type": "number",
        "forms": [
          {
            "href": "http://localhost:8080/linear-stage/motor-temperature/warning",
            "op": "readproperty",
            "htv:methodName": "GET",
            "contentType": "application/json"
          },
          {
            "href": "http://localhost:8080/linear-stage/motor-temperature/warning",
            "op": "writeproperty",
            "htv:methodName": "PUT",
            "contentType": "application/json"
          }
        ],
        "minimum": 30
      },
      "motor_shutoff_temperature": {
        "title": "motor_shutoff_temperature",
        "description": "Temperature (in degree Celcius) above which the axis shuts down.If the motor warmed up to a defined temperature value,the controller switches off and the power stage must bereset.",
        "type": "number",
        "forms": [
          {
            "href": "http://localhost:8080/linear-stage/motor-temperature/shut-off",
            "op": "readproperty",
            "htv:methodName": "GET",
            "contentType": "application/json"
          },
          {
            "href": "http://localhost:8080/linear-stage/motor-temperature/shut-off",
            "op": "writeproperty",
            "htv:methodName": "PUT",
            "contentType": "application/json"
          }
        ],
        "minimum": 40
      },
      "resolver_voltage": {
        "title": "resolver_voltage",
        "description": "n=3...10V",
        "type": "number",
        "forms": [
          {
            "href": "http://localhost:8080/linear-stage/resolver-voltage",
            "op": "readproperty",
            "htv:methodName": "GET",
            "contentType": "application/json"
          },
          {
            "href": "http://localhost:8080/linear-stage/resolver-voltage",
            "op": "writeproperty",
            "htv:methodName": "PUT",
            "contentType": "application/json"
          }
        ],
        "minimum": 3,
        "maximum": 10
      },
      "resolver_ratio": {
        "title": "resolver_ratio",
        "description": "Resolver ratio (ratio of primary to secondary winding), 0=1/8, 1=1/4,2=1/2, 3=1, 4=2",
        "type": "integer",
        "enum": [
          0,
          1,
          2,
          3,
          4
        ],
        "forms": [
          {
            "href": "http://localhost:8080/linear-stage/resolver-ratio",
            "op": "readproperty",
            "htv:methodName": "GET",
            "contentType": "application/json"
          },
          {
            "href": "http://localhost:8080/linear-stage/resolver-ratio",
            "op": "writeproperty",
            "htv:methodName": "PUT",
            "contentType": "application/json"
          }
        ]
      },
      "is_referenced": {
        "title": "is_referenced",
        "description": "is the motor currently referenced?",
        "readOnly": true,
        "type": "boolean",
        "forms": [
          {
            "href": "http://localhost:8080/linear-stage/referenced",
            "op": "readproperty",
            "htv:methodName": "GET",
            "contentType": "application/json"
          }
        ]
      },
      "is_in_position": {
        "title": "is_in_position",
        "description": "is the motor in last commanded position?",
        "readOnly": true,
        "type": "boolean",
        "forms": [
          {
            "href": "http://localhost:8080/linear-stage/positioned",
            "op": "readproperty",
            "htv:methodName": "GET",
            "contentType": "application/json"
          }
        ]
      },
      "is_in_hard_negative_limit": {
        "title": "is_in_hard_negative_limit",
        "description": "is the motor in negative limit?",
        "readOnly": true,
        "type": "boolean",
        "forms": [
          {
            "href": "http://localhost:8080/linear-stage/positioned/hardware-negative",
            "op": "readproperty",
            "htv:methodName": "GET",
            "contentType": "application/json"
          }
        ]
      },
      "is_in_hard_positive_limit": {
        "title": "is_in_hard_positive_limit",
        "description": "is the motor in negative limit?",
        "readOnly": true,
        "type": "boolean",
        "forms": [
          {
            "href": "http://localhost:8080/linear-stage/positioned/hardware-positive",
            "op": "readproperty",
            "htv:methodName": "GET",
            "contentType": "application/json"
          }
        ]
      },
      "is_in_soft_negative_limit": {
        "title": "is_in_soft_negative_limit",
        "description": "is the motor in software negative limit?",
        "readOnly": true,
        "type": "boolean",
        "forms": [
          {
            "href": "http://localhost:8080/linear-stage/positioned/software-negative",
            "op": "readproperty",
            "htv:methodName": "GET",
            "contentType": "application/json"
          }
        ]
      },
      "is_in_soft_positive_limit": {
        "title": "is_in_soft_positive_limit",
        "description": "is the motor in software positive limit?",
        "readOnly": true,
        "type": "boolean",
        "forms": [
          {
            "href": "http://localhost:8080/linear-stage/positioned/software-positive",
            "op": "readproperty",
            "htv:methodName": "GET",
            "contentType": "application/json"
          }
        ]
      },
      "is_moving": {
        "title": "is_moving",
        "description": "is the motor moving?",
        "readOnly": true,
        "type": "boolean",
        "forms": [
          {
            "href": "http://localhost:8080/linear-stage/moving",
            "op": "readproperty",
            "htv:methodName": "GET",
            "contentType": "application/json"
          }
        ]
      }
    },
    "actions": {
      "define_position": {
        "title": "define_position",
        "description": "overwrite current position without moving axis",
        "forms": [
          {
            "href": "http://localhost:8080/linear-stage/position/override",
            "op": "invokeaction",
            "htv:methodName": "POST",
            "contentType": "application/json"
          }
        ],
        "safe": true,
        "idempotent": true,
        "synchronous": true
      },
      "emergency_stop_move": {
        "title": "emergency_stop_move",
        "description": "Emergency stop, harder deceleration ramp",
        "forms": [
          {
            "href": "http://localhost:8080/linear-stage/motion/emergency-stop",
            "op": "invokeaction",
            "htv:methodName": "POST",
            "contentType": "application/json"
          }
        ],
        "safe": true,
        "idempotent": true,
        "synchronous": true
      },
      "exit": {
        "title": "exit",
        "description": "Exit the object without killing the eventloop that runs this object. If Thing wasstarted using the run() method, the eventloop is also killed. This method canonly be called remotely.",
        "forms": [
          {
            "href": "http://localhost:8080/linear-stage/exit",
            "op": "invokeaction",
            "htv:methodName": "POST",
            "contentType": "application/json"
          }
        ],
        "safe": true,
        "idempotent": true,
        "synchronous": true
      },
      "get_postman_collection": {
        "title": "get_postman_collection",
        "description": "organised postman collection for this object",
        "forms": [
          {
            "href": "http://localhost:8080/linear-stage/resources/postman-collection",
            "op": "invokeaction",
            "htv:methodName": "GET",
            "contentType": "application/json"
          }
        ],
        "safe": true,
        "idempotent": true,
        "synchronous": true
      },
      "get_thing_description": {
        "title": "get_thing_description",
        "description": "generate thing description schema of Web of Things https://www.w3.org/TR/wot-thing-description11/.one can use the node-wot as a client for the object with the generated schema(https://github.com/eclipse-thingweb/node-wot). Other WoT related tools based on TD will be compatible.Composed Things that are not the top level object is currently not supported.Parameters----------authority: str, optionalprotocol with DNS or protocol with hostname+port, for example 'https://my-pc:8080' or'http://my-pc:9090' or 'https://IT-given-domain-name'. If absent, a value will be automaticallygiven using ``socket.gethostname()`` and the port at which the last HTTPServer (``hololinked.server.HTTPServer``)attached to this object was running.Returns:hololinked.wot.td.ThingDescriptionrepresented as an object in python, gets automatically serialized to JSON when pushed out of the socket.",
        "forms": [
          {
            "href": "http://localhost:8080/linear-stage/resources/wot-td",
            "op": "invokeaction",
            "htv:methodName": "GET",
            "contentType": "application/json"
          }
        ],
        "safe": true,
        "idempotent": true,
        "synchronous": true
      },
      "goto_limit_neg": {
        "title": "goto_limit_neg",
        "forms": [
          {
            "href": "http://localhost:8080/linear-stage/motion/limit-neg",
            "op": "invokeaction",
            "htv:methodName": "POST",
            "contentType": "application/json"
          }
        ],
        "safe": true,
        "idempotent": true,
        "synchronous": true
      },
      "goto_limit_pos": {
        "title": "goto_limit_pos",
        "forms": [
          {
            "href": "http://localhost:8080/linear-stage/motion/limit-pos",
            "op": "invokeaction",
            "htv:methodName": "POST",
            "contentType": "application/json"
          }
        ],
        "safe": true,
        "idempotent": true,
        "synchronous": true
      },
      "load_properties_from_DB": {
        "title": "load_properties_from_DB",
        "description": "Load and apply parameter values which have ``db_init`` or ``db_persist``set to ``True`` from database",
        "forms": [
          {
            "href": "http://localhost:8080/linear-stage/properties/db-reload",
            "op": "invokeaction",
            "htv:methodName": "POST",
            "contentType": "application/json"
          }
        ],
        "safe": true,
        "idempotent": true,
        "synchronous": true
      },
      "move_relative": {
        "title": "move_relative",
        "forms": [
          {
            "href": "http://localhost:8080/linear-stage/motion/relative",
            "op": "invokeaction",
            "htv:methodName": "POST",
            "contentType": "application/json"
          }
        ],
        "safe": true,
        "idempotent": true,
        "synchronous": true
      },
      "reference_negative": {
        "title": "reference_negative",
        "forms": [
          {
            "href": "http://localhost:8080/linear-stage/reference/negative",
            "op": "invokeaction",
            "htv:methodName": "POST",
            "contentType": "application/json"
          }
        ],
        "safe": true,
        "idempotent": true,
        "synchronous": true
      },
      "reference_positive": {
        "title": "reference_positive",
        "forms": [
          {
            "href": "http://localhost:8080/linear-stage/reference/positive",
            "op": "invokeaction",
            "htv:methodName": "POST",
            "contentType": "application/json"
          }
        ],
        "safe": true,
        "idempotent": true,
        "synchronous": true
      },
      "reset_status": {
        "title": "reset_status",
        "forms": [
          {
            "href": "http://localhost:8080/linear-stage/reset-hardware-status",
            "op": "invokeaction",
            "htv:methodName": "POST",
            "contentType": "application/json"
          }
        ],
        "safe": true,
        "idempotent": true,
        "synchronous": true
      },
      "run_backward": {
        "title": "run_backward",
        "description": "Danger! Use cautiously, if connection is lost no way to stop!",
        "forms": [
          {
            "href": "http://localhost:8080/linear-stage/motion/backward",
            "op": "invokeaction",
            "htv:methodName": "POST",
            "contentType": "application/json"
          }
        ],
        "safe": true,
        "idempotent": true,
        "synchronous": true
      },
      "run_forward": {
        "title": "run_forward",
        "description": "Danger! Use cautiously, if connection is lost no way to stop!",
        "forms": [
          {
            "href": "http://localhost:8080/linear-stage/motion/forward",
            "op": "invokeaction",
            "htv:methodName": "POST",
            "contentType": "application/json"
          }
        ],
        "safe": true,
        "idempotent": true,
        "synchronous": true
      },
      "step_backward": {
        "title": "step_backward",
        "forms": [
          {
            "href": "http://localhost:8080/linear-stage/motion/single-step-backward",
            "op": "invokeaction",
            "htv:methodName": "POST",
            "contentType": "application/json"
          }
        ],
        "safe": true,
        "idempotent": true,
        "synchronous": true
      },
      "step_forward": {
        "title": "step_forward",
        "forms": [
          {
            "href": "http://localhost:8080/linear-stage/motion/single-step-forward",
            "op": "invokeaction",
            "htv:methodName": "POST",
            "contentType": "application/json"
          }
        ],
        "safe": true,
        "idempotent": true,
        "synchronous": true
      },
      "stop_move": {
        "title": "stop_move",
        "forms": [
          {
            "href": "http://localhost:8080/linear-stage/motion/stop",
            "op": "invokeaction",
            "htv:methodName": "POST",
            "contentType": "application/json"
          }
        ],
        "safe": true,
        "idempotent": true,
        "synchronous": true
      }
    },
    "events": {
      
    },
    "security": "unimplemented",
    "securityDefinitions": {
      "unimplemented": {
        "scheme": "nosec",
        "description": "currently no security scheme supported - use cookie auth directly on hololinked.server.HTTPServer object"
      }
    }
  }