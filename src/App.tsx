import { useEffect, useState, createContext, useCallback } from 'react';

import '@fontsource/roboto/300.css';
import '@fontsource/roboto/400.css';
import '@fontsource/roboto/500.css';
import '@fontsource/roboto/700.css';
import './lib/wot-bundle.min.js'
import SettingsIcon from '@mui/icons-material/Settings';
import InsertDriveFileIcon from '@mui/icons-material/InsertDriveFile';
import HistoryIcon from '@mui/icons-material/History';
import DnsIcon from '@mui/icons-material/Dns';

import { MovementOptions } from './components/movements';
import { RefreshStatus, Status } from './components/status.js';
import { MotorSettings } from './components/motor-settings.js';
import { ServerInfo } from './components/server.js';
import { PositionHistory } from './components/position-history.js';
import { TDViewer } from './components/td-viewer.js';
import { BottomDrawer } from './layout/drawer.js';
import { sampleTD } from './interactions/sample-td.js';
import { sleep } from './utils/index.js';
import { movementControlDefaultProperties, settingsDefaultProps, 
        statusDefaultProps, updatePropertiesFromPythonNames } from './interactions/properties.js';



export const ClientContext = createContext<any>(null)

export const TDContext = createContext<any>(null)

export const ServerContext = createContext<any>(null)

export const SettingsPropertiesContext = createContext<any>(null)

export const StatusPropertiesContext = createContext<any>(null)

export const MovementControlPropertiesContext = createContext<any>(null)



function App() {

    const [TD, setTD] = useState<any>(sampleTD)
    const [server, setServer] = useState<string>(import.meta.env.VITE_DEFAULT_PHYMOTION_SERVER)
    const [client, setClient] = useState(null)

    const [statusProperties, setStatusProperties] = useState<any>(statusDefaultProps)
    const [movementControlProperties, setMovementControlProperties] = useState<any>(movementControlDefaultProperties)
    const [settingsProperties, setSettingsProperties] = useState<any>(settingsDefaultProps)

    const pollThingProperties = useCallback(async (reschedule : boolean) => {
        console.log("polling properties from ", client)
        if (client) {
            try {
                let props = await client.readAllProperties()
                console.log("read properties from server")
                let [statusProps, movementControlProps, settingsProps] = await updatePropertiesFromPythonNames(props)
                setStatusProperties(statusProps) // condition setState, not compatible with React hooks, verify later
                setMovementControlProperties(movementControlProps)
                setSettingsProperties(settingsProps)
            } catch (error) {
                console.error("Cannot read properties from server");
                console.error(error);
            }
        }
        if (reschedule && client) {
            await sleep(10000)
            requestAnimationFrame(() => pollThingProperties(true))
            console.log("rescheduled polling of properties") 
        }
    }, [client])


    useEffect(() => {
        const startServient = async() => {
            let servient = new Wot.Core.Servient(); 
            servient.addClientFactory(new Wot.Http.HttpsClientFactory({ allowSelfSigned : true }))
            let WoT = await servient.start()
            console.info("WoT servient started")
            
            let td = await WoT.requestThingDescription(`${server}/resources/wot-td`)
            let client = await WoT.consume(td);
        
            console.info("consumed thing description from phymotion controller")
            setClient(client)
            setTD(td)
        }
        startServient()
    }, [server])

    useEffect(() => {
        requestAnimationFrame(() => pollThingProperties(true))
    }, [client])

    return (
        <ClientContext.Provider value={client}>
            <TDContext.Provider value={[TD, setTD]}>
                <StatusPropertiesContext.Provider value={statusProperties}>
                    <Status />
                </StatusPropertiesContext.Provider>
                <MovementControlPropertiesContext.Provider value={movementControlProperties}>
                    <MovementOptions properties={movementControlProperties}/>
                </MovementControlPropertiesContext.Provider>
                <RefreshStatus pollThingProperties={pollThingProperties} />
                <ServerContext.Provider value={[server, setServer]}>
                    <SettingsPropertiesContext.Provider value={settingsProperties}>
                        <BottomDrawer actions={allowedActions} />
                    </SettingsPropertiesContext.Provider>
                </ServerContext.Provider>
            </TDContext.Provider>
        </ClientContext.Provider>
    )
}


const allowedActions = [
    {
        name : 'Motor Settings',
        icon : <SettingsIcon />,
        child : MotorSettings
    },
    {
        name : 'TD Viewer',
        icon : <InsertDriveFileIcon />,
        child : TDViewer
    },
    {
        name : 'Position History',
        icon : <HistoryIcon />,
        child : PositionHistory
    },
    {
        name : 'Change Server',
        icon : <DnsIcon />,
        child : ServerInfo
    }

]

export default App
