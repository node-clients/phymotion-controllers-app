import { SwipeableDrawer, SpeedDial, SpeedDialAction, SpeedDialIcon } from "@mui/material"
import { fabStyle } from './styles';
import { useState } from "react";
import { MotorSettings } from "../components/motor-settings";


type SpeedDialActionType = {
    name : string
    icon : JSX.Element
    child : JSX.Element
}

export const BottomDrawer = ({ actions } : { actions : Array<any> }) => {

    const [drawersOpen, setDrawerOpen] = useState(actions.map((_ : SpeedDialActionType) => false));

    const toggleDrawer = (open : boolean, currentIndex : number) => {
        let drawerStatus = actions.map((_ : SpeedDialActionType, index : number) => {
            if (index === currentIndex && open)
                return true 
            return false 
        })
        setDrawerOpen(drawerStatus)
    };

    return (
        <>
            <SpeedDial
                ariaLabel="options" 
                sx={fabStyle} icon={<SpeedDialIcon />}
            >
                {actions.map((action : SpeedDialActionType, index : number) => 
                    <SpeedDialAction
                        key={action.name}
                        icon={action.icon}
                        tooltipTitle={action.name} 
                        onClick={() => toggleDrawer(true, index)}
                    />
                )}
            </SpeedDial>
            {actions.map((action : SpeedDialActionType, index : number) => 
                <SwipeableDrawer
                    key={action.name}
                    onOpen={() => {}}
                    anchor="bottom"
                    open={drawersOpen[index]}
                    onClose={() => toggleDrawer(false, index)}
                    sx={{ 
                        '& .MuiDrawer-paper': {
                            height: '80%',
                            position: "fixed",
                            width: '100%'
                        }
                    }}
                    transitionDuration={250}
                >
                    { action.child === MotorSettings? <MotorSettings /> : action.child() }
                </SwipeableDrawer>
            )}
        </>
    )
}

