import { Box, Button, ButtonGroup, IconButton, Stack, TextField, Typography } from "@mui/material"
import { useContext, useCallback, useState, createContext, useEffect } from "react"
import SendIcon from '@mui/icons-material/Send';

import { ClientContext, MovementControlPropertiesContext } from "../App"
import { BorderBox } from "../layout/border-box"
import { LCDIndicator } from "../layout/lcd";
import { positionSpinnerStyles } from "../layout/styles";
import { movementControlDefaultProperties } from "../interactions/properties";



const CurrentPosition = () => {

    const { positionRead } = useContext(MovementControlPropertiesContext);
    const [positionReadFinal, setPositionReadFinal] = useState<string | number>('UNKNOWN');
    const motor = useContext(ClientContext)

    useEffect(() =>{
        if (motor) {
            const updatePosition = async (data) => {
                try {
                    data.ignoreValidation = true
                    let val = await data.value()
                    setPositionReadFinal(val)
                    console.log("Position changed to:", val) 
                } catch (error) {
                    console.error("Cannot read the observed property position");
                    console.error(error);
                }

            }
            const subscribe = async() => {
                await motor.observeProperty('position', updatePosition)
                console.log("subscribed to motor position")
            } 
            subscribe()
        }
    }, [motor])

    useEffect(() => {   
        setPositionReadFinal(positionRead)
    }, [positionRead]) 

    return (
        <Stack direction="row" sx={{ paddingTop : 2, minHeight : 60, maxHeight: 60 }}>
            <Typography sx={{ minWidth: 80, fontSize: 18, paddingRight : 1 }}>Position <br/> (abs)</Typography>
            <LCDIndicator value={positionReadFinal} fontSize={40} />
        </Stack>
    )
}


export const FixedMovements = () => {

    const motor = useContext(ClientContext)
    const [position, setPosition] = useState<number | string>(1.0)

    const handlePositionChange = useCallback((event : React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
        setPosition(event.target.value === '' ? '' : Number(event.target.value))
    }, [setPosition])

    const commandPosition = useCallback(async() => {
        await motor.writeProperty('position', { value : position })
        console.debug(`commanded position ${position}`)
    }, [position, motor])

    const stopMoving = useCallback(async() => {
        await motor.invokeAction('stop_move')
        console.debug("called stop move")
    }, [position, motor])

    return (
        <Stack direction="row" sx={{ paddingTop : 2, minHeight: 56, maxHeight: 56, display: 'flex', flexGrow : 1 }}> 
            <TextField 
                sx={{ 
                    paddingRight : 1, alignSelf : "flex-start",
                    "input[type=number]::-webkit-inner-spin-button": {
                        ...positionSpinnerStyles
                    },
                    "input[type=number]::fontSize": 24,
                    display: 'flex', flexGrow : 1
                }} 
                type="number" 
                value={position}
                onChange={handlePositionChange}
            />
            <IconButton size="large" sx={{ backgroundColor: '#F2F2F2' }} onClick={commandPosition} >
                <SendIcon fontSize="large" />
            </IconButton>
            <Box sx={{ paddingLeft : 1 }}></Box>
            <Button color="error" variant="contained" onClick={stopMoving} sx={{ fontSize : 24 }}>
                Stop
            </Button>
        </Stack>        
    )
}


export const RelativeMovements = () => {

    const motor = useContext(ClientContext)
    const [relativePosition, setRelativePosition] = useState<any>(1.0)

    const handleRelativePositionChange = useCallback((event : React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
        setRelativePosition(event.target.value === '' ? '' : Number(event.target.value))
    }, [setRelativePosition])

    const moveRelative = useCallback(async(direction : string) => {
        await motor.invokeAction('move_relative', { offset : direction == "positive"? relativePosition : -relativePosition })
        console.debug(`called relative move ${relativePosition} towards ${direction}`)
    }, [relativePosition, motor])

    return (
        <Stack direction="row" sx={{ paddingTop : 2, minHeight: 56, maxHeight: 56 }}>
            <Typography sx={{ fontSize : 18, minWidth: 80, maxWidth: 80 }} >
                Position (rel)
            </Typography>
            <Button 
                variant="outlined" 
                sx={{ fontSize : 30, fontWeight : 900 }}
                onClick={() => moveRelative("negative")}
            >
                -
            </Button>
            <TextField 
                sx={{ 
                    paddingLeft: 1, paddingRight : 1, alignSelf : "flex-start",
                    "input[type=number]::-webkit-inner-spin-button": {
                        ...positionSpinnerStyles
                    },
                    "input[type=number]::fontSize": 24,
                }}
                type="number"
                value={relativePosition}
                onChange={handleRelativePositionChange}      
            />
            <Button 
                variant="outlined" 
                sx={{ fontSize : 30, fontWeight : 900 }}
                onClick={() => moveRelative("positive")}
            >
                +
            </Button>
        </Stack>
    )
}


export const FavouritePositions = () => {

    const { favouritePositions, favouritePositionName } = useContext(MovementControlPropertiesContext)
   
    return (
        <Stack sx={{ paddingTop : 2 }}>
            <Typography sx={{ fontSize : 18 }}>
                Favourite Positions
                { favouritePositions.length > 0?  '' : ' : None Found' }
            </Typography>
            <div >
                {favouritePositions.map((name : string) => {
                    return (
                        <Button 
                            key={name} variant="outlined"  
                            sx={{ 
                                borderRadius : 0, fontSize : 18,
                                backgroundColor : name === favouritePositionName? 'yellow' : null,
                                color : name === favouritePositionName? 'black' : null
                            }}
                        >
                            {name}
                        </Button>                       
                    )
                })}
            </div>
        </Stack>
    )
}


export const StandardMovements = () => {

    const motor = useContext(ClientContext)

    const startPositiveRef = useCallback(async() => {
        await motor.invokeAction('reference_positive')
        console.debug("called reference positive")
    }, [motor])

    const startNegativeRef = useCallback(async() => {
        await motor.invokeAction('reference_negative')
        console.debug("called reference negative")
    }, [motor])

    const gotoLimitPositive  = useCallback(async() => {
        await motor.invokeAction('goto_limit_pos')
        console.debug("called goto limit positive")
    }, [motor])

    const gotoLimitNegative  = useCallback(async() => {
        await motor.invokeAction('goto_limit_neg')
        console.debug("called goto limit negative")
    }, [motor])

    return (
        <Box sx={{ display: 'flex', gap: '8px', maxHeight: 40, justifyContent : 'space-between'}}>
            <ButtonGroup>
                <Button onClick={startPositiveRef} sx={{ fontSize : 18 }}>
                    Ref +
                </Button>
                <Button onClick={startNegativeRef} sx={{ fontSize : 18 }}>
                    Ref -
                </Button>
            </ButtonGroup>
            <ButtonGroup>
                <Button onClick={gotoLimitPositive} sx={{ fontSize : 18 }}>
                    Limit +
                </Button>
                <Button onClick={gotoLimitNegative} sx={{ fontSize : 18 }}>
                    Limit -
                </Button>
            </ButtonGroup>
        </Box>
    )
}


export const MovementOptions = ({ properties } : { properties : any }) => {

    const [props, setProps] = useState<any>(movementControlDefaultProperties)

    useEffect(() => {
        setProps(properties)
    }, [properties])

    return (
        <Stack direction="row" sx={{ paddingTop : 2 }}>
            <BorderBox text="Movement Control">
                <StandardMovements />
                <CurrentPosition />
                <FixedMovements />
                <RelativeMovements />
                <FavouritePositions />               
            </BorderBox>
        </Stack>
    )
}