import { Box, Typography } from "@mui/material";
import "../../assets/lcd-fonts.css"


export const LCDIndicator = ({ value, fontSize } : { value : string | number, fontSize : number }) => {
    return (
        <Box sx={{ display: 'flex', flexGrow: 1 }} >
            <Typography 
                variant="h4" component="span"
                sx={{ 
                    fontSize: fontSize, letterSpacing: '0.2rem', fontFamily: 'digital-7', 
                    boxShadow: '0 0 10px rgba(0, 255, 0, 0.5)', alignItems: 'center', 
                    justifyContent : 'right', display: 'flex', flexGrow: 1, backgroundColor: '#0f0', color: 'black',
                }}
            >
                {value}
            </Typography>
        </Box>
    );
};
  
