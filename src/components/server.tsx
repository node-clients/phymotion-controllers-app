import { useContext, useState } from "react"
import { Button, TextField, Typography, Stack, CircularProgress, IconButton } from "@mui/material"
import OpenInNew from "@mui/icons-material/OpenInNew"
import { ServerContext} from "../App"
import { Padding } from "../layout/styles"



export const ServerInfo = () => {

    const [server, setServer] = useContext(ServerContext)
    const [newServer, setNewServer] = useState<string>(server)
    const [lastPing, setLastPing] = useState<string>('UNKNOWN')
    const [pinging, setPinging] = useState<boolean>(false)

    const ping = async () => {
        setPinging(true)
        let pingInfo = ''
        try {
            const response = await fetch(`${newServer}/ping`, { method : 'POST' })   
            pingInfo = new Date().toISOString().replace('T', ' ').replace('Z', '').slice(0, -1) + '.' + String(new Date().getMilliseconds()).padStart(3, '0')
            if(response.ok) 
                pingInfo += ', OK'
            else
                pingInfo += ', DIDNT WORK'
        } catch (error) {
            pingInfo = 'ERROR'
            console.error(error)
        }
        setLastPing(pingInfo)
        setPinging(false)
    }

    const load = async () => {  
        let finalServer = server
        try {
            const response = await fetch(`${newServer}/resources/wot-td`, { method : 'GET' })
            if(response.ok) {
                const td = await response.json()
                finalServer = newServer 
            }
        } catch (error) {
            console.error(error)
        }
        setServer(finalServer)
    }

    return (
        <Stack sx={{ paddingLeft : 2 }}>
            <TextField 
                multiline rows={3} 
                sx={{ padding : 2, paddingLeft : 0 }}  
                value={newServer}        
                onChange={(event : React.ChangeEvent<HTMLInputElement>) => setNewServer(event.target.value)}        
            />
            <Stack direction="row" sx={{ paddingBottom : 2 }} >
                <Button variant="contained" sx={{ maxWidth : 50 }} onClick={ping}>
                    Ping
                </Button>
                <Padding value={1} />
                <Button variant="contained" sx={{ maxWidth : 50 }} onClick={load}>
                    Load
                </Button>
                <Padding value={1} />   
                <IconButton onClick={() => window.open(`${newServer}/resources/wot-td`, '_blank')} >
                    <OpenInNew />
                </IconButton>
                {pinging? <CircularProgress size="small" /> : null}
            </Stack>
            <Typography>Last Ping : {lastPing}</Typography>
        </Stack>
    )
}