import { createContext, useContext, useEffect, useState } from "react"
import { Stack, Typography, Box, ButtonGroup, Button } from "@mui/material"
import { BorderBox } from "../layout/border-box"
import { StatusPropertiesContext } from "../App"

const stateColorMap : { [key : string] : string } = {
    ON : "white",
    FAULT : "red",
    MOVING : "yellow",
    DISABLED : "grey",
    DISCONNECTED : "orange"
}


export const Info = () => {

    const { state, status, driveID, axis } = useContext(StatusPropertiesContext) 

    return (
        <>
            {/* <Stack direction="row">
                <Typography variant="button" sx={{ paddingRight : 1, width : 70, fontSize: 18 }}>State :</Typography>
                <Typography variant="button" sx={{ backgroundColor : stateColorMap[state], fontSize: 18}}>{state}</Typography>
            </Stack>
            <Stack direction="row">
                <Typography variant="button" sx={{ paddingRight : 1, minWidth: 80, fontSize: 18 }}>Status :</Typography>
                <Typography sx={{fontSize: 18}}>{status}</Typography>
            </Stack> */}
            <Stack direction="row">
                <Typography variant="button" sx={{ paddingRight : 1, minWidth: 80, fontSize: 18 }}>Device :</Typography>
                <Typography sx={{fontSize: 18, pt : 0.25}}>Slot {driveID}, Axis {axis}</Typography>
            </Stack>
        </>
    )
}

export const Limits = () => {

    const { isInSoftNegativeLimit, isInSoftPositiveLimit, 
        isInHardNegativeLimit, isInHardPositiveLimit,
        isReferenced, isPositionOverriden } = useContext(StatusPropertiesContext)

    return (
        <Stack>
            <ButtonGroup sx={{ paddingTop : 1 }}>
                <Button sx={{ fontSize : 18, backgroundColor : isInSoftPositiveLimit? 'yellow' : '' }} >
                    Soft Lim + 
                </Button>
                <Button sx={{ fontSize : 18, backgroundColor : isInSoftNegativeLimit? 'yellow' : '' }}>
                    Soft Lim - 
                </Button>
            </ButtonGroup>
            <ButtonGroup sx={{ paddingTop : 1 }}>
                <Button sx={{ fontSize : 18, backgroundColor : isInHardPositiveLimit? 'yellow' : '' }}>
                    Lim + 
                </Button>
                <Button sx={{ fontSize : 18, backgroundColor : isInHardNegativeLimit? 'yellow' : '' }}>
                    Lim - 
                </Button>
            </ButtonGroup>
            <ButtonGroup sx= {{ paddingTop : 1}}>
                <Button sx={{ 
                    fontSize : 18, 
                    backgroundColor : isReferenced? 'green' : '', 
                    color : isReferenced? 'black' : ''
                }}>
                    Referenced
                </Button>
                <Button sx={{ 
                    fontSize : 18, 
                    backgroundColor : isPositionOverriden? 'orange' : '', 
                    color : isPositionOverriden? 'black' : ''
                }}>
                    Position Overriden 
                </Button>
            </ButtonGroup>
        </Stack>
    )

}

export const RefreshStatus = ({ pollThingProperties } : { pollThingProperties : any}) => {

    return (
        <Box sx={{ paddingTop : 1 }} >
            <Button color="secondary" variant="contained" sx={{ fontSize : 18 }} onClick={pollThingProperties}>
                Refresh
            </Button>
        </Box>
    )
}


export const Status = () => {

    return (
        <BorderBox text="Status">
            <Info />
            <Limits />            
        </BorderBox>
    )
}