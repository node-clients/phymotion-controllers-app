# Phytron Phymotion Mobile App

Example app written in node-wot & React. 

Works with server implemented [here](https://gitlab.com/hololinked-examples/phymotion-controllers)

<p float="left">
    <img src="/assets/main-view.png" width="49%" />
    <img src="/assets/td-viewer.png" width="49%" /> 
</p>


