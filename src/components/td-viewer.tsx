import { useContext, useState } from "react"
import { Autocomplete, AutocompleteChangeReason, Box, Button, ButtonGroup, TextField } from "@mui/material"
import ReactJson from "react-json-view"
import { TDContext } from "../App"


export const TDViewer = () => {

    const [option, setOption] = useState<string>('properties')
    const [affordance, setAffordance] = useState<string>('')
    const [TD, _] = useContext(TDContext)

    return (
        <>
            <ButtonGroup sx={{ padding : 2 }}>
                <Button 
                    onClick={() => setOption('properties')}
                    sx={{ backgroundColor : option === 'properties'? 'yellow' : '' }}    
                >Properties</Button>
                <Button 
                    onClick={() => setOption('actions')}
                    sx={{ backgroundColor : option === 'actions' ? 'yellow' : '' }}  
                >Actions</Button>
                <Button 
                    onClick={() => setOption('events')}
                    sx={{ backgroundColor : option === 'events'? 'yellow' : '' }}  
                >Events</Button>
            </ButtonGroup>
            <Autocomplete
                disablePortal
                // @ts-ignore
                options={Object.keys(TD[option as keyof TD])}
                sx={{ padding : 2 }}
                onChange={(event : React.SyntheticEvent, value : string | null, reason : AutocompleteChangeReason) => 
                        setAffordance(value as string)}
                renderInput={(params) => <TextField {...params} label={option} />}
            />
            {affordance? TD[option as keyof TD][affordance as string]?
                    <Box sx={{ padding : 2, overflow: 'auto' }}>
                        <ReactJson 
                            src={TD[option as keyof TD][affordance as string]} 
                            name={affordance}
                            displayDataTypes={false}
                        ></ReactJson>
                    </Box>
                    : null : null 
            }     
        </>
    )
}