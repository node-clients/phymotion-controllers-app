import { Box } from "@mui/material";

export const positionSpinnerStyles = {
    border : "1px",
    width : "2rem",
    opacity: 1,
    position: "absolute",
    top: 0,
    right: 0,
    bottom: 0,
    overflow: "hidden",
    borderTopRightRadius: "0.5em",
    borderBottomRightRadius: "0.5em",
}

export const fabStyle = {
    position: 'absolute',
    bottom: 30,
    right: 30,
};


export const modalStyle = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    width : '50%',
    transform: 'translate(-50%, -50%)',
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
};


export const Padding = ( { value = 2 } : { value : number}) => {

    return (
        <Box sx={{ padding : value }}></Box>
    )
}