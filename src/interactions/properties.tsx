export type statusPropertiesType = {
    state : string
    status : string
    driveID : string 
    axis : string 
    isReferenced : boolean
    isInHardNegativeLimit : boolean
    isInHardPositiveLimit : boolean
    isInSoftNegativeLimit : boolean
    isInSoftPositiveLimit : boolean
    isPositionOverriden : boolean
    isMoving : boolean
    isInPosition : boolean 
}

export let statusDefaultProps : statusPropertiesType = {
    state : 'DISCONNECTED',
    status : 'Disconnected to server, use connection wizard in settings',
    driveID : '',
    axis : '', 
    isReferenced : false,
    isInHardNegativeLimit : false,
    isInHardPositiveLimit : false,
    isInSoftNegativeLimit : false,
    isInSoftPositiveLimit : false,
    isPositionOverriden : false,
    isMoving : false,
    isInPosition : false, 
}



export type movementControlPropertiesType = {
    positionRead : number | string 
    favouritePositionName : number | string
    favouritePositions : string[]
}

export let movementControlDefaultProperties : movementControlPropertiesType = {
    positionRead : 'UNKNOWN',
    favouritePositionName : 'UNKNOWN',
    favouritePositions : []
}
        


export type settingsPropsType = {
    movementType : number 
    positionUnits : number 
    conversionFactor : number
    startStopFrequency : number
    emergencyStopFrequency : number
    emergencyStopRamp : number
    referencingRunFrequency : number
    referencingAccelerationFrequency : number 
    referencingLeaveFrequency : number 
    referencingLimitPlusOffset : number 
    referencingLimitMinusOffset : number
    referencingRecoveryTime : number 
    runFrequency : number
    acceleration : number 
    positionRecoveryTime : number
    boostEnable : boolean
    encoderDeviationCounter : number 
    absoluteCounter : number
    encoderCounter : number
    softLimitMax : number
    softLimitMin : number
    playCompensation : number
    encoderDataRate : number
    limitSwitchType : number
    enabled : boolean
    enabledAtBoot : boolean
    frequencyBand : number
    controlPredivider : number 
    accelerationRampShape : number 
    accelerationRampArc : number 
    encoderType : number
    encoderResolution : number
    encoderFunction : number
    encoderSFITolerance : number
    encoderPreferentialDirection : number
    encoderConversion : number
    stopCurrent : number
    runCurrent : number
    boostCurrent : number
    runcurrentHoldTime : number
    controlPulseOrigin : number
    stepResolution : number
    powerStageTemperature : number
    controlDivider : number
    pulseWidth : number
    powerStageMonitoring : boolean 
    motorTemperature : number 
    motorWarningTemperature : number
    motorShutoffTemperature : number
    resolverVoltage : number
    resolverRatio : number 
    
}

export let settingsDefaultProps: settingsPropsType = {
    movementType: 0,
    positionUnits: 0,
    conversionFactor: 0,
    startStopFrequency: 0,
    emergencyStopFrequency: 0,
    emergencyStopRamp : 0,
    referencingRunFrequency: 0,
    referencingAccelerationFrequency: 0,
    referencingLeaveFrequency: 0,
    referencingLimitPlusOffset: 0,
    referencingLimitMinusOffset: 0,
    referencingRecoveryTime: 0,
    runFrequency: 0,
    acceleration: 0,
    positionRecoveryTime: 0,
    boostEnable: false,
    absoluteCounter: 0,
    encoderCounter : 0,
    encoderDeviationCounter: 0,
    softLimitMax: 0,
    softLimitMin: 0,
    playCompensation: 0,
    encoderDataRate: 0,
    limitSwitchType: 0,
    enabled: false,
    enabledAtBoot: false,
    frequencyBand: 0,
    controlPredivider: 0,
    accelerationRampShape: 0,
    accelerationRampArc: 0,
    encoderType: 0,
    encoderResolution: 0,
    encoderFunction: 0,
    encoderSFITolerance: 0,
    encoderPreferentialDirection: 0,
    encoderConversion: 0,
    stopCurrent: 0,
    runCurrent: 0,
    boostCurrent: 0,
    runcurrentHoldTime: 0,
    controlPulseOrigin: 0,
    stepResolution: 0,
    powerStageTemperature: 0,
    controlDivider: 0,
    pulseWidth: 0,
    powerStageMonitoring: false,
    motorTemperature: 0,
    motorWarningTemperature: 0,
    motorShutoffTemperature: 0,
    resolverVoltage: 0,
    resolverRatio: 0,
};



export const underscoreNames = Object.keys(settingsDefaultProps).map(key => key.replace(/([A-Z])/g, "_$1").toLowerCase());

export function getCamelCaseName(underscoreName : string) {
    return underscoreName.replace(/_./g, (x) => x[1].toUpperCase())
}

// console.log("underscore names", underscoreNames)


    
export async function updatePropertiesFromPythonNames(properties : any) {
    
    console.log("fetched properties are ", properties)
    let statusProps : statusPropertiesType = {...statusDefaultProps}
    let movementControlProps : movementControlPropertiesType = {...movementControlDefaultProperties}
    let settingsProps : settingsPropsType = {...settingsDefaultProps}
            
    for(let [name, interactionOutput] of properties) {
        try {
            switch(name) {
                case 'state' : statusProps.state = await interactionOutput.value(); break;
                case 'status' : statusProps.status = await interactionOutput.value(); break;
                case 'drive_ID' : statusProps.driveID = await interactionOutput.value(); break;
                case 'axis' : statusProps.axis = await interactionOutput.value(); break;
                case 'is_referenced' : statusProps.isReferenced = await interactionOutput.value(); break;
                case 'is_in_hard_negative_limit' : statusProps.isInHardNegativeLimit = await interactionOutput.value(); break;
                case 'is_in_hard_positive_limit' : statusProps.isInHardPositiveLimit = await interactionOutput.value(); break;
                case 'is_in_soft_negative_limit' : statusProps.isInSoftNegativeLimit = await interactionOutput.value(); break;
                case 'is_in_soft_positive_limit' : statusProps.isInSoftPositiveLimit = await interactionOutput.value(); break;
                case 'is_position_overriden' : statusProps.isPositionOverriden = await interactionOutput.value(); break;
                case 'is_in_position' : statusProps.isInPosition = await interactionOutput.value(); break;
                case 'is_moving' : statusProps.isMoving = await interactionOutput.value(); break;

                case 'position' : movementControlProps.positionRead = await interactionOutput.value(); break;
               
                case 'movement_type' : settingsProps.movementType = await interactionOutput.value(); break;
                case 'position_units' : settingsProps.positionUnits = await interactionOutput.value(); break;
                case 'conversion_factor' : settingsProps.conversionFactor = await interactionOutput.value(); break;
                case 'start_stop_frequency' : settingsProps.startStopFrequency = await interactionOutput.value(); break;
                case 'emergency_stop_frequency' : settingsProps.emergencyStopFrequency = await interactionOutput.value(); break;
                case 'emergency_stop_ramp' : settingsProps.emergencyStopRamp = await interactionOutput.value(); break; 
                case 'referencing_run_frequency' : settingsProps.referencingRunFrequency = await interactionOutput.value(); break;
                case 'referencing_acceleration_frequency' : settingsProps.referencingAccelerationFrequency = await interactionOutput.value(); break;
                case 'referencing_leave_frequency' : settingsProps.referencingLeaveFrequency = await interactionOutput.value(); break;
                case 'referencing_limit_plus_offset' : settingsProps.referencingLimitPlusOffset = await interactionOutput.value(); break;
                case 'referencing_limit_minus_offset' : settingsProps.referencingLimitMinusOffset = await interactionOutput.value(); break;
                case 'referencing_recovery_time' : settingsProps.referencingRecoveryTime = await interactionOutput.value(); break;
                case 'run_frequency' : settingsProps.runFrequency = await interactionOutput.value(); break;
                case 'acceleration' : settingsProps.acceleration = await interactionOutput.value(); break;
                case 'position_recovery_time' : settingsProps.positionRecoveryTime = await interactionOutput.value(); break;
                case 'boost_enable' : settingsProps.boostEnable = await interactionOutput.value(); break;
                case 'absolute_counter' : settingsProps.absoluteCounter = await interactionOutput.value(); break;
                case 'encoder_counter' : settingsProps.encoderCounter = await interactionOutput.value(); break; 
                case 'encoder_deviation_counter' : settingsProps.encoderDeviationCounter = await interactionOutput.value(); break;
                case 'soft_limit_max' : settingsProps.softLimitMax = await interactionOutput.value(); break;
                case 'soft_limit_min' : settingsProps.softLimitMin = await interactionOutput.value(); break;
                case 'play_compensation' : settingsProps.playCompensation = await interactionOutput.value(); break;
                case 'encoder_data_rate' : settingsProps.encoderDataRate = await interactionOutput.value(); break;
                case 'limit_switch_type' : settingsProps.limitSwitchType = await interactionOutput.value(); break;
                case 'enabled' : settingsProps.enabled = await interactionOutput.value(); break;
                case 'enabled_at_boot' : settingsProps.enabledAtBoot = await interactionOutput.value(); break;
                case 'frequency_band' : settingsProps.frequencyBand = await interactionOutput.value(); break;
                case 'control_predivider' : settingsProps.controlPredivider = await interactionOutput.value(); break;
                case 'acceleration_ramp_shape' : settingsProps.accelerationRampShape = await interactionOutput.value(); break;
                case 'acceleration_ramp_arc' : settingsProps.accelerationRampArc = await interactionOutput.value(); break;
                case 'encoder_type' : settingsProps.encoderType = await interactionOutput.value(); break;
                case 'encoder_resolution' : settingsProps.encoderResolution = await interactionOutput.value(); break;
                case 'encoder_function' : settingsProps.encoderFunction = await interactionOutput.value(); break;
                case 'encoder_SFI_tolerance' : settingsProps.encoderSFITolerance = await interactionOutput.value(); break;
                case 'encoder_preferential_direction' : settingsProps.encoderPreferentialDirection = await interactionOutput.value(); break;
                case 'encoder_conversion' : settingsProps.encoderConversion = await interactionOutput.value(); break;
                case 'stop_current' : settingsProps.stopCurrent = await interactionOutput.value(); break;
                case 'run_current' : settingsProps.runCurrent = await interactionOutput.value(); break;
                case 'boost_current' : settingsProps.boostCurrent = await interactionOutput.value(); break;
                case 'runcurrent_hold_time' : settingsProps.runcurrentHoldTime = await interactionOutput.value(); break;
                case 'control_pulse_origin' : settingsProps.controlPulseOrigin = await interactionOutput.value(); break;
                case 'step_resolution' : settingsProps.stepResolution = await interactionOutput.value(); break;
                case 'power_stage_temperature' : settingsProps.powerStageTemperature = await interactionOutput.value(); break;
                case 'control_divider' : settingsProps.controlDivider = await interactionOutput.value(); break;
                case 'pulse_width' : settingsProps.pulseWidth = await interactionOutput.value(); break;
                case 'power_stage_monitoring' : settingsProps.powerStageMonitoring = await interactionOutput.value(); break;
                case 'motor_temperature' : settingsProps.motorTemperature = await interactionOutput.value(); break;
                case 'motor_warning_temperature' : interactionOutput.ignoreValidation = true, settingsProps.motorWarningTemperature = await interactionOutput.value(); break;
                case 'motor_shutoff_temperature' : interactionOutput.ignoreValidation = true, settingsProps.motorShutoffTemperature = await interactionOutput.value(); break;
                case 'resolver_voltage' : interactionOutput.ignoreValidation = true, settingsProps.resolverVoltage = await interactionOutput.value(); break;
                case 'resolver_ratio' : settingsProps.resolverRatio = await interactionOutput.value(); break;

                default : break; console.error("could not find property ", name);
            }
        } catch(error) {
            console.error("Error property ", name)
            console.error(error)
        }
    }

    console.log("status properties", statusProps)
    console.log("movement control properties", movementControlProps)
    console.log("settings properties", settingsProps)
   
    return [statusProps, movementControlProps, settingsProps]
}


