import { Box, Typography } from "@mui/material";

export const BorderBox = ({ text, children } : { text : string, children : JSX.Element | JSX.Element[]}) => {
    
    return (
        <Box
            sx={{ 
                position: 'relative', border: '1px solid grey', borderRadius: '4px', padding: 2, 
            }}
        >
            <Typography 
                variant="caption" 
                sx={{ 
                    position: 'absolute', top: '-10px', left: '10px', 
                    background: 'white', padding: '0 4px', fontSize : 14 
                }}
            >
                {text}
            </Typography>
            {children}
        </Box>
    )
}
